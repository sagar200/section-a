import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Menubar from "./components/navbar/Menubar";
import Content from "./components/content/Content";

function App() {
  return (
    <div className="App">
      <Menubar />
      <Content />
    </div>
  );
}

export default App;
