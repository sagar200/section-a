import React from "react";
import "./content.css";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/esm/Button";
import DialogueBox from "../modal/DialogueBox";
import RangeSlider from "react-bootstrap-range-slider";
import classNames from "classnames";

import { useState } from "react";

function Content() {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [userValue, setUserValue] = useState(1);

  const highlighter = (e) => {
    let value = e.target.value;
    setUserValue(value);
  };

  return (
    <div className="body">
      <div className="headline">
        <h1 className="display-1">Pricing</h1>
        <p className="headline-text">
          Quickly build an effective pricing table for your potential customers
          with this Bootstrap example. It's built with default Bootstrap
          components and utilities with little customization.
        </p>
      </div>
      <div className="card-container">
        <Card
          className={classNames("card", { "card-highlight": userValue <= 10 })}
        >
          <Card.Header className="card-header">Free</Card.Header>
          <Card.Body className="card-body">
            <Card.Title className="card-title">$0 / mo</Card.Title>
            <Card.Text className="card-text">
              10 users included 2 GB of storage Email support Help center access
            </Card.Text>
            <Button onClick={handleShow} className="button" variant="primary">
              Sign Up for Free
            </Button>
          </Card.Body>
        </Card>

        <Card
          className={classNames("card", {
            "card-highlight": userValue > 10 && userValue <= 20,
          })}
        >
          <Card.Header className="card-header">Pro</Card.Header>
          <Card.Body className="card-body">
            <Card.Title className="card-title">$15 / mo</Card.Title>
            <Card.Text className="card-text">
              20 users included 10 GB of storage Priority email support Help
              center access
            </Card.Text>
            <Button onClick={handleShow} className="button" variant="primary">
              Get Started
            </Button>
          </Card.Body>
        </Card>

        <Card
          className={classNames("card", {
            "card-highlight": userValue > 20 && userValue <= 30,
          })}
        >
          <Card.Header className="card-header">Enterprise</Card.Header>
          <Card.Body className="card-body">
            <Card.Title className="card-title">$29 / mo</Card.Title>
            <Card.Text className="card-text">
              30 users included 15 GB of storage Phone and email support Help
              center access
            </Card.Text>
            <Button onClick={handleShow} className="button" variant="primary">
              Contact Us
            </Button>
          </Card.Body>
        </Card>
      </div>
      <div className="slider-container">
        <p className="display-4 slider-headline">Filter your plan:</p>
        <RangeSlider
          className="slider"
          value={userValue}
          min="1"
          max="30"
          onChange={(changeEvent) => highlighter(changeEvent)}
        />
      </div>
      <DialogueBox show={show} handleClose={handleClose} />
    </div>
  );
}

export default Content;
