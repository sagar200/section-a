import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import Axios from "axios";
import "../content/Content";
import { useState } from "react";
const url = "https://forms.maakeetoo.com/formapi/119";

function DialogueBox({ show, handleClose }) {
  const [data, setData] = useState({
    firstname: "",
    email: "",
    message: "",
  });

  const handle = (event) => {
    const newdata = { ...data };
    newdata[event.target.id] = event.target.value;
    setData(newdata);
    console.log(newdata);
  };

  const submitHandler = async (e) => {
    await Axios.post(url, {
      firstname: data.firstname,
      email: data.email,
      message: data.message,
    })
      .then((response) => {
        alert("Details sent");
        window.location.reload(false);
      })
      .catch((error) => {
        alert("Invalid Input");
        window.location.reload(false);
        console.log(error);
      });

    console.log("submitted" + data.firstname, data.email, data.message);
  };
  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Enter Details</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <Form>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="firstname">Name*</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter the name"
                id="firstname"
                name="firstname"
                onChange={(event) => handle(event)}
                value={data.firstname}
                autoFocus
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="email">Email address*</Form.Label>
              <Form.Control
                type="email"
                placeholder="name@example.com"
                name="email"
                id="email"
                onChange={(event) => handle(event)}
                value={data.email}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="message">Order comments*</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                name="message"
                id="message"
                onChange={(event) => handle(event)}
                value={data.message}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>* fields are mandatory.</Form.Label>
            </Form.Group>
          </Form>
        </Modal.Body>

        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button
            variant="primary"
            type="submit"
            onClick={(e) => submitHandler(e)}
          >
            Send Details
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default DialogueBox;
