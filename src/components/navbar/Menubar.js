import React, { useState } from "react";
import "./navbar.css";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Button from "react-bootstrap/esm/Button";
import DialogueBox from "../modal/DialogueBox";

function Menubar() {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <>
      <Navbar collapseOnSelect expand="lg" bg="light">
        <Container>
          <Navbar.Brand href="#pricing" className="me-auto">
            Cloud Storage Services
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto"></Nav>
            <Nav>
              <Nav.Link className="link" href="#features">
                Features
              </Nav.Link>
              <Nav.Link className="link" href="#pricing">
                Pricing
              </Nav.Link>
              <Nav.Link className="link" href="#support">
                Support
              </Nav.Link>
              <Nav.Link className="link" href="#enterprise">
                Enterprise
              </Nav.Link>

              <Button onClick={handleShow}>Sign Up</Button>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <DialogueBox show={show} handleClose={handleClose} />
    </>
  );
}

export default Menubar;
